import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'layout-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  @Input('isCollapsed')
  isCollapsed: boolean;

  constructor() {}

  ngOnInit() {}
}
