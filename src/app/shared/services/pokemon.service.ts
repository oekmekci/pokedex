import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  Observable,
  of
} from "rxjs";

import { environment } from '@env/environment';
import { SharedModule } from '@shared/shared.module';

const baseUrl = `${environment.apiBaseUrl}/pokemon/`;

@Injectable({
  providedIn: SharedModule
})
export class PokemonService {
  pokemons;
  pokemonDetails = new Map();

  constructor(private http: HttpClient) {
  }

  getPokemonList(): Observable<any> {
    // delivering cached pokemons in case if they were prefetched
    if (this.pokemons) {
      return of(this.pokemons);
    } else {
      return new Observable(obs => {
        this.http.get<any>(`${baseUrl}`).subscribe(result => {
          this.pokemons = result;
          obs.next(result);
        });
      })
    }
  }

  get(id): Observable<any> {
    if (this.pokemonDetails.has(id)) {
      return this.pokemonDetails.get(id);
    } else {
      return new Observable(pokemonDetails => {
        this.http.get<any>(`${baseUrl}${id}/`).subscribe(result => {
          this.pokemonDetails.set(id, result);
          pokemonDetails.next(result);
        });
      })
    }

  }

}
