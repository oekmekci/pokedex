import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import {StorageService} from "@core/services/storage.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  activeUser;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) { }

  getAuthenticatedUser(): Observable<any> {
    const token = this.storageService.get('access_token');
    if (token) {
      const user = {
        name: 'user',
        access_token: token
      };

      this.activeUser = user;
      return of(user);
    } else {
      return of(null);
    }
  }
}
