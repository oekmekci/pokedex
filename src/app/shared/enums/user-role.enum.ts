export enum UserRoleEnum {
  ROLE_ADMIN = 1,
  ROLE_USER,
  ROLE_COMPANY_ADMIN,
  ROLE_VIEWER
}
