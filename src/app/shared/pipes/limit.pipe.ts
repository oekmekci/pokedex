import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'limit',
  pure: true
})
export class LimitPipe implements PipeTransform {

  transform(value: any[], limit: string): any {
    const limitValue = parseInt(limit, 10);

    if (!value) {
      return;
    }

    if (Number.isNaN(limitValue)) {
      return value;
    } else if (Number.isInteger(limitValue)) {
      return value.slice(0, limitValue);
    }
  }
}
