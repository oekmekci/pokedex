import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';

import { LayoutDefaultComponent } from './default/default.component';
import { SidebarComponent } from './default/sidebar/sidebar.component';

const COMPONENTS = [
  LayoutDefaultComponent,
  SidebarComponent,
];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    ...COMPONENTS,
  ],
  exports: [
    ...COMPONENTS
  ]
})
export class LayoutModule { }
