import {getPropertyByKeyPath} from '@shared/utils/object.util';

export function searchFieldsOnTable(dataTable, keyword, fields) {
  const lowerCaseKeyword = keyword.toLowerCase();

  return dataTable.filter(key => {
    const searchFields = [];

    fields.forEach( field => {
      let fieldValue;
      if (field.includes('.')) {
        fieldValue = getPropertyByKeyPath(key, field);
      } else {
        fieldValue = key[field];
      }

      if (fieldValue) {
        searchFields.push(fieldValue);
      }
    });
    
    return searchFields.filter(item => {
      if (item != null && typeof item === 'object') {
        return Object.values(item).some(value => value.toString().toLowerCase().includes(lowerCaseKeyword));
      } else {
        return item && item.toString().toLowerCase().includes(lowerCaseKeyword);
      }
    }).length;
  });
}
