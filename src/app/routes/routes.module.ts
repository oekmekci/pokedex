import { NgModule } from '@angular/core';

import { SharedModule } from '@shared/shared.module';
import { RoutesRoutingModule } from './routes-routing.module';

import { DetailComponent } from './details/detail.component';
import { ListComponent } from './list/list.component';
import { LoginComponent } from "./login/login.component";


const COMPONENTS = [
  // page components
  ListComponent,
  LoginComponent,
  DetailComponent
];

@NgModule({
  imports: [
    SharedModule,
    RoutesRoutingModule
  ],
  declarations: [
    ...COMPONENTS
  ]
})
export class RoutesModule {}
