import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { LimitPipe } from './pipes/limit.pipe';
import { TruncateTextPipe } from './pipes/truncate-text.pipe';

// region: third libs
import { NgZorroAntdModule } from 'ng-zorro-antd';
const THIRDMODULES = [
  NgZorroAntdModule,
];
// endregion

// region: your components & directives
const COMPONENTS = [];
const DIRECTIVES = [];
const PIPES = [
  LimitPipe,
  TruncateTextPipe
];
// endregion

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    // third libs
    ...THIRDMODULES
  ],
  declarations: [
    // components and directives
    ...COMPONENTS,
    ...DIRECTIVES,
    ...PIPES,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    // third libs
    ...THIRDMODULES,
    // components and directives
    ...COMPONENTS,
    ...DIRECTIVES,
    ...PIPES
  ]
})
export class SharedModule { }
