import { Component,  OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {Router} from "@angular/router";
import {AuthenticationService} from "@core/services/authentication.service";
import {UserService} from "@core/services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginResponseWaiting = false;

  constructor(
      private fb: FormBuilder,
      private authService: AuthenticationService,
      private router: Router,
      private userService: UserService
      ) {
  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      userName: [ null, [ Validators.required ] ],
      password: [ null, [ Validators.required ] ],
      remember: [ true ]
    });
  }

  validateForm(form: FormGroup): void {
    for (const i in form.controls) {
      if (form.controls.hasOwnProperty(i)) {
        form.controls[i].markAsDirty();
        form.controls[i].updateValueAndValidity();
      }
    }
  }

  submitForm(): void {
    this.validateForm(this.loginForm);

    if (this.loginForm.invalid) {
      return;
    }

    this.loginResponseWaiting = true;

    const formData = this.loginForm.value;

    this.authService.generateToken(formData).subscribe(token => {
      this.loginResponseWaiting = false;
      this.userService.activeUser = {name: 'John', surname: 'Doe'};
      this.router.navigate(['/list']);
    });
  }

}
