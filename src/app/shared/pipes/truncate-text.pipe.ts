import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncateText'
})
export class TruncateTextPipe implements PipeTransform {
  transform(value: string, length: number): any {
    const ellipsis = '...';
    const result = Array.from(value);

    return result.length > length ? `${result.slice(0, length).join('')}${ellipsis}` : value;
  }
}
