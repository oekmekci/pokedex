import { Injectable } from '@angular/core';

import { AuthenticationService } from '@core/services/authentication.service';
import { UserService } from '@core/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class SetupService {

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService) { }

  private static getParameterByName(name: string, url: string): string {
    url = url || window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');

    const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
    const results = regex.exec(url);

    if (!results) {
      return null;
    }
    if (!results[2]) {
      return '';
    }

    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  initialize(): Promise<void> {
    return new Promise((resolve, reject) => {

      if (this.authenticationService.isTokenActive()) {
        this.userService.getAuthenticatedUser().subscribe(() => {
          resolve();
        });
      }

      resolve();
    });
  }

}
