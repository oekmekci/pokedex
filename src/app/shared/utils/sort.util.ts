import {getPropertyByKeyPath} from '@shared/utils/object.util';

export function sortFieldsOnTable(dataTable, e) {
  const keyword = e.key;
  const [...newData] = dataTable;

  if (keyword.split('.').length) {
    return newData.sort(
      (a, b) => {
        const key1 = getPropertyByKeyPath(a, keyword);
        const key2 = getPropertyByKeyPath(b, keyword);
        return (e.value === 'ascend') ? (key1 > key2 ? 1 : -1) : (key2 > key1 ? 1 : -1);
      });
  } else {
    return newData.sort(
      (a, b) => (e.value === 'ascend') ? (a[e.key] > b[e.key] ? 1 : -1) : (b[e.key] > a[e.key] ? 1 : -1)
    );
  }
}
