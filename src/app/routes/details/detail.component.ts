import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { PokemonService } from "@shared/services/pokemon.service";




@Component({
  selector: 'app-poke-detail',
  templateUrl: './detail.component.html'
})
export class DetailComponent implements OnInit {
  id;
  pokemon;
  isLoading = true;


  constructor(
    private route: ActivatedRoute,
    private pokeService: PokemonService
  ) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.queryParamMap.get('id');
    this.getPokemonDetails();
  }

  getPokemonDetails() {
    this.pokeService.get(this.id).subscribe(pokemon => {
      console.log(pokemon);
      this.pokemon = pokemon;
      this.isLoading = false;
    });
  }

}
