import {getPropertyByKeyPath} from '@shared/utils/object.util';

export function generateFilterArray(sourceArray, targetArray, text, value, keyPrefix = null): Array<any> {
  // This function generates an array from dataTable for the ant table filter source based on the text and value

  return sourceArray.reduce((acc, item) => {
    const filterText = getPropertyByKeyPath(item, text);
    const filterValue = getPropertyByKeyPath(item, value);

    if (typeof filterText !== 'undefined' && filterText !== null && typeof filterValue !== 'undefined' && filterValue !== null) {
      return acc.concat({
        text: typeof filterText !== 'undefined' ? filterText : text,
        value: typeof filterValue !== 'undefined' ? filterValue : value,
        key: keyPrefix ? `${keyPrefix}.${value}` : value
      });
    } else {
      return acc;
    }
  }, []);
}

export function generateUniqueFilterArray(sourceArray, targetArray, text, value, keyPrefix = null): Array<any> {
  // This method returns only unique values from the generated Filter Array
  return Array.from(
    new Set(
      generateFilterArray(sourceArray, targetArray, text, value, keyPrefix).map(item => JSON.stringify(item))
    )
  ).map( item => JSON.parse(item));
}


export function generateSelectedFilterArray(selectedArray, filterArray): Array<any> {
  // when user selects a filter from filterArray ant returns string Array
  // This method returns only selected filter Array
  return filterArray.filter(
    item => selectedArray.some(selected => selected === item.value)
  );
}

export function filterDisplayData(displayData, allFilters): Array<any> {
  // displayData => [ {id: 1, type: "CHROME", version: "13", operating_system: {..} }, ...]
  // allFilters => {type: [{text: 'CHROME', value: CHROME}], operating_system: [..]}

  const filteredDisplayData = displayData.reduce((acc, displayItem) => {
    let matches = true;
    // let includesCurrentFilter = false;

    for (const filter in allFilters) {
      if (allFilters.hasOwnProperty(filter)) {
        const currentFilters = allFilters[filter];
        // currentFilters => [{ text: 'CHROME', value: 'CHROME', key: 'type' }, { text: 'FIREFOX', value: 'FIREFOX', key: 'type' },]
        matches = currentFilters.some(currentFilter => {
          // currentFilter => { text: 'CHROME', value: 'CHROME', key: 'type' }
          // OR currentFilter => { text: 'Windows 10', value: 'WIN10', key: 'operating_system.code' }
          let tableItemValue;

          if (currentFilter.key.includes('.')) {
            tableItemValue = getPropertyByKeyPath(displayItem, currentFilter.key);
          } else {
            tableItemValue = displayItem[currentFilter.key];
          }

          if (tableItemValue === currentFilter.value) {
            return true;
          }

          matches = false;
          return false;
        });

        if (!matches) {
          break;
        }
      }
    }

    if (matches) {
      return acc.concat(displayItem);
    } else {
      return acc;
    }

  }, []);

  return filteredDisplayData;
}

export function filterFieldsOnTable(selected, filters, key) {
  if (!selected.length) {
    if (this.allFilters.hasOwnProperty(key)) {
      delete this.allFilters[key];
      this.displayData = filterDisplayData(this.tableData, this.allFilters);
      return;
    }

    if (!Object.keys(this.allFilters).length) {
      this.displayData = this.tableData;
      return;
    }
  }

  this.allFilters = {
    ...this.allFilters,
    [key]: generateSelectedFilterArray(selected, filters)
  };
  this.displayData = filterDisplayData(this.tableData, this.allFilters);
}
