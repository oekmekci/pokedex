import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { StorageService } from '@core/services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(
    private http: HttpClient,
    private storageService: StorageService,
    ) {

  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.log(`${operation} failed: ${error.message}`);

      if (operation === 'generateToken') {
        this.logOut();
        this.goToLogin();
      }

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  goToLogin(): void {
    // TODO route to the login page

  }

  logOut(): void {
    this.storageService.clear();
  }

  generateToken(params: string) {
    const token = this.guid(params);
    this.storageService.set('access_token', token);

    // Imitating the request delay to illustrate the loading process
    return of(token).pipe(
        delay(400)
    );
  }

  isTokenActive(): boolean {
    if (this.storageService.get('access_token')) {
      return  true;
    }
    return false;
  }


  guid(formData) {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  getAuthorizationToken(): string | null {
    return this.storageService.get('access_token');
  }
}
