export function getPropertyByKeyPath(targetObj, keyPath) {
  const keys = keyPath.split('.');
  keys.reverse();

  if (!keys.length) {
    return;
  }

  let result = targetObj;

  while (keys.length) {
    const key = keys.pop();

    if (result.hasOwnProperty(key)) {
      result = result[key];
    } else {
      return undefined;
    }
  }

  return result;
}
