import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '@env/environment';
import { SharedModule } from '@shared/shared.module';

const baseUrl = `${environment.apiBaseUrl}platform/dashboards`;

@Injectable({
  providedIn: SharedModule
})
export class DashboardService {

  constructor(private http: HttpClient) {
  }

  get(id): Observable<any> {
    return this.http.get<any>(`${baseUrl}/${id}`);
  }

  list(): Observable<any[]> {
    return this.http.get<any[]>(`${baseUrl}`);
  }

  create(dashboard: any): Observable<any> {
    return this.http.post<any>(`${baseUrl}`, dashboard);
  }

  update(dashboard: any): Observable<any> {
    return this.http.put<any>(`${baseUrl}`, dashboard);
  }

  delete(dashboard: any): Observable<any> {
    return this.http.delete<any>(`${baseUrl}/${dashboard.id}`);
  }

  // widget create
  createWidget(dashboardId, widget): Observable<any> {
    return this.http.post<any>(`${baseUrl}/${dashboardId}/widgets`, widget);
  }
}
