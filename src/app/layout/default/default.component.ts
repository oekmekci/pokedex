import { Component, OnInit } from '@angular/core';
import {
  NavigationEnd,
  NavigationError,
  NavigationStart,
  RouteConfigLoadStart,
  Router
} from '@angular/router';

import { AuthenticationService } from '@core/services/authentication.service';
import { UserService } from '@core/services/user.service';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'layout-default',
  templateUrl: './default.component.html'
})
export class LayoutDefaultComponent implements OnInit {
  isFetching = false;
  isCollapsed = false;
  triggerTemplate = null;
  activeUser;

  constructor(
    private router: Router,
    private _message: NzMessageService,
    public userService: UserService,
    public authenticationService: AuthenticationService
  ) {
    router.events.subscribe(evt => {
      if (!this.isFetching && evt instanceof RouteConfigLoadStart) {
        this.isFetching = true;
      }
      if (evt instanceof NavigationError) {
        this.isFetching = false;
        _message.error(`Unable to load ${evt.url} routing`, {
          nzDuration: 1000 * 3
        });
        return;
      }
      if (evt instanceof NavigationStart) {
        window.scroll({
          top: 0,
          left: 0,
          behavior: 'smooth'
        });
      }
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      setTimeout((): void => {
        this.isFetching = false;
      }, 200);
    });

    this.activeUser = this.userService.activeUser;
  }

  ngOnInit(): void {}

  logout(): void {
    this.authenticationService.logOut();
    this.userService.activeUser = null;
    this.router.navigate(['/login']);
  }
}
