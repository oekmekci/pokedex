import { FormGroup } from '@angular/forms';

export function validateFormGroup(form: FormGroup): void {
  for (const i in form.controls) {
    if (form.controls.hasOwnProperty(i)) {
      form.controls[i].markAsDirty();
      form.controls[i].updateValueAndValidity();
    }
  }
}
