import {
  HttpErrorResponse,
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent,
} from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, of, throwError } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';

import { AuthenticationService } from '@core/services/authentication.service';
import { NzMessageService, NzNotificationService } from 'ng-zorro-antd';

/**
 * The default HTTP interceptor
 */
@Injectable()
export class DefaultInterceptor implements HttpInterceptor {
  constructor(
    private injector: Injector,
    private authenticationService: AuthenticationService,
    private notificationService: NzNotificationService
  ) {
  }

  get msg(): NzMessageService {
    return this.injector.get(NzMessageService);
  }

  private goTo(url: string) {
    setTimeout(() => this.injector.get(Router).navigateByUrl(url));
  }

  private handleData(event: HttpResponse<any> | HttpErrorResponse): Observable<any> {
    // Business Rules
    switch (event.status) {
      case 200:
      case 201:
      case 202:
      case 203:
      case 204:
        // Business level error handling
        // The following code snippet can be applied directly
        // if (event instanceof HttpResponse) {
        //     const body: any = event.body;
        //     if (body && body.status !== 0) {
        //         this.msg.error(body.msg);
        //         // Continue to throw the error interrupt all subsequent Pipe、subscribe operation, therefore：
        //         // this.http.get('/').subscribe() Does not trigger
        //         return throwError({});
        //     } else {
        //         // Re-edit the `body` content to `response` content, no longer need to care about the business status code
        //            for most scenarios
        //         return of(new HttpResponse(Object.assign(event, { body: body.response })));
        //         // Or still maintain the full format
        //         return of(event);
        //     }
        // }

        if (event instanceof HttpErrorResponse) {
          this.msg.error(event.message);
          return throwError(event);
        }
        break;
      case 401: // unauthorized request
        this.authenticationService.goToLogin();
        break;
      case 403:
      case 404:
      case 500:
        // TODO: 404 and 500 error pages are missing
        this.goTo(`/${event.status}`);
        break;
      default:
        if (event instanceof HttpErrorResponse) {
          this.msg.error(event.message);
        }
        return throwError(event);
    }
    return of(event);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<| HttpSentEvent
    | HttpHeaderResponse
    | HttpProgressEvent
    | HttpResponse<any>
    | HttpUserEvent<any>> {

    return next.handle(req).pipe(
      mergeMap((event: any) => {
        // Allows uniform handling of request errors, because a request if the status of its HTTP request is 200 if it is a business error
        if (event instanceof HttpResponse && event.status.toString().startsWith('20')) {

          // Global Notification Service for CRUD operations
          let title: string;

          if (req.headers.has('x-no-notification')) {
            return this.handleData(event);
          }

          if (req.method === 'PUT') {
            title = 'Update Successful';
          }

          if (req.method === 'POST') {
            title = 'Create Successful';
          }

          if (req.method === 'DELETE') {
            title = 'Delete Successful';
          }

          if (req.method === 'PUT' || req.method === 'POST' || req.method === 'DELETE') {
            if (!req.url.endsWith('token')) {
              this.notificationService.success(title, null);
            }
          }

          return this.handleData(event);
        }
        // If everything is ok, follow-up
        return of(event);
      }),
      catchError((err: HttpErrorResponse) => {

        // Global Notification Service for error handling of CRUD operations
        let title: string;

        if (req.method === 'PUT') {
          title = 'Update Failed';
        }

        if (req.method === 'POST') {
          title = 'Create Failed';
        }

        if (req.method === 'DELETE') {
          title = 'Delete Failed';
        }

        if (err.status === 409) {
          title = 'Record already exist!';
        }

        if (req.method === 'PUT' || req.method === 'POST' || req.method === 'DELETE' || err.status === 409) {
          this.notificationService.error(title, 'Oops there was en error with the operation');
        }

        return this.handleData(err);
      })
    );
  }
}
