import { Component, OnInit } from "@angular/core";

import {
  FormBuilder,
  FormGroup
} from "@angular/forms";
import { PokemonService } from "@shared/services/pokemon.service";

import { getPropertyByKeyPath } from '@shared/utils/object.util';

@Component({
  selector: 'app-poke-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit {
  searchForm: FormGroup;

  // Table
  tableData: any[];
  displayData: any[];
  tableLoading = false;


  constructor(
      private pokeService: PokemonService,
      private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.searchForm = this.fb.group({
      searchText: [null],
    });

    this.getPokemons();
  }


  getPokemons() {
    this.pokeService.getPokemonList().subscribe(result => {
      this.tableData = result.results;
      this.displayData = [...result.results];
      this.tableLoading = false;
    });
  }

  search(keyword) {
    if (!keyword.length) {
      this.displayData = [...this.tableData];
      return;
    }

    const lowerCaseKeyword = keyword.toLowerCase();

    const searchData = this.tableData.filter(key => {
      return [
        key.name
      ].filter(item => {
        if (item != null && typeof item === 'object') {
          return Object.values(item).some(value => value.toString().toLowerCase().includes(lowerCaseKeyword));
        } else {
          return item.toString().toLowerCase().includes(lowerCaseKeyword);
        }
      }).length;
    });

    this.displayData = searchData;
  }

  sort(e) {
    if (!e.value) {
      this.displayData = this.tableData;
      return;
    }

    const keyword = e.key;
    const [...newData] = this.displayData;

    if (keyword.split('.').length) {
      this.displayData = newData.sort(
        (a, b) => {
          const key1 = getPropertyByKeyPath(a, keyword);
          const key2 = getPropertyByKeyPath(b, keyword);
          return (e.value === 'ascend') ? (key1 > key2 ? 1 : -1) : (key2 > key1 ? 1 : -1);
        });
    } else {
      this.displayData = newData.sort(
        (a, b) => (e.value === 'ascend') ? (a[e.key] > b[e.key] ? 1 : -1) : (b[e.key] > a[e.key] ? 1 : -1)
      );
    }
  }
}
