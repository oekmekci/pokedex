import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// layout
import { LayoutDefaultComponent } from '../layout/default/default.component';

// pages
import { ListComponent } from './list/list.component';

// guards
import { AuthGuard } from '@core/security/auth.guard';

// Routes
import { DetailComponent } from "./details/detail.component";
import {LoginComponent} from "./login/login.component";

const routes: Routes = [
  {
    path: '',
    component: LayoutDefaultComponent,
    children: [
      { path: '', redirectTo: 'dashboards', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'list', canActivate: [AuthGuard], component: ListComponent },
      { path: 'details', canActivate: [AuthGuard], component: DetailComponent }
    ],
    data: { roles: [/*UserRoleEnum.ROLE_ADMIN*/] }
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutesRoutingModule {
}
